import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler

class LoadSoundDataset(object):
	def __init__(self,path,num_actor=18,sample_per_actor=60):
		super().__init__()
		self.train_actor = num_actor
		self.samplepa = sample_per_actor
		self.path = path
		self.load()
		self.process()

	def process(self,):
		l_num_actor = self.data['num_actor'].unique()
		coltotransform = ['chroma_stft', 'rms', 'spectral_centroid',
			'spectral_bandwidth', 'rolloff', 'zero_crossing_rate', 'mfcc0', 'mfcc1',
			'mfcc2', 'mfcc3', 'mfcc4', 'mfcc5', 'mfcc6', 'mfcc7', 'mfcc8', 'mfcc9',
			'mfcc10', 'mfcc11', 'mfcc12', 'mfcc13', 'mfcc14', 'mfcc15', 'mfcc16',
			'mfcc17', 'mfcc18', 'mfcc19', 'mfcc20', 'mfcc21', 'mfcc22', 'mfcc23',
			'mfcc24', 'mfcc25', 'mfcc26', 'mfcc27', 'mfcc28', 'mfcc29', 'mfcc30',
			'mfcc31', 'mfcc32', 'mfcc33', 'mfcc34', 'mfcc35', 'mfcc36', 'mfcc37',
			'mfcc38']

		for it_num_actor in l_num_actor:
			test_sample = self.data.loc[self.data['num_actor']==it_num_actor][coltotransform]
			t_ss = StandardScaler()
			scale_sample = t_ss.fit_transform(test_sample.values)
			self.data.loc[self.data['num_actor']==it_num_actor,coltotransform] = scale_sample

		self.y = self.data.pop('target')

		self.y_train = self.y.loc[:(self.train_actor*self.samplepa)-1].values
		self.y_test = self.y.loc[self.train_actor*self.samplepa:].values

		self.data = self.data.drop(['num_actor'],axis=1)

		self.X = self.data.iloc[:,2:]
		self.X_train = self.X.loc[:(self.train_actor*self.samplepa)-1].values
		self.X_test = self.X.loc[self.train_actor*self.samplepa:].values

		self.X_train = np.expand_dims(self.X_train,axis=2)
		self.X_test = np.expand_dims(self.X_test,axis=2)

	def load(self):
		self.data = pd.read_csv(self.path,sep=';')
		self.data = self.data.sort_values(by=['num_actor'],ascending=False)
		self.data.reset_index(inplace=True)